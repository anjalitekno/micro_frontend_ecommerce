import React from 'react'
import Footer from 'home/Footer'
import Header from 'home/Header'
import PDPContent from './PDPContent'
import { BrowserRouter as Router , Route, Routes, Switch } from "react-router-dom";

const Pdpmaini = () => {
    return (
        <div>
            <Router>
                <Header />
                
                {/* <Login /> */}
                <Routes>
                    <Route path="/product/:id" Component={PDPContent} />
                </Routes>
                <Footer />
            </Router>
           
        </div>
    )
}

export default Pdpmaini
