import React from "react";
import ReactDOM from "react-dom";
import "remixicon/fonts/remixicon.css"


import "./index.css";
import Pdpmaini from "./Pdpmaini";

const App = () => (
  <div className="container">
  <Pdpmaini/>
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
