import React, { useEffect, useState } from "react";
import { getProduct, currency } from "home/products";
import { getProductById } from "../../home/src/products";
import { useParams } from "react-router-dom";
export default function PDPContent() {
    const { id } = useParams();
    // const id =1
    const [product, setProduct] = useState(null);

    useEffect(() => {
        if (id) {
            getProductById(id).then(setProduct);
        } else {
            setProduct(null)
        }
    }, [id])

    if (!product) return null;

    return (
        <div className="addTocardDive">
            <img src={product.image} alt="" />
            <p className="textitle"> {product.title}</p>

        </div>

    )
}