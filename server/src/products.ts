export interface Product {
    id: number;
    product_name: string;
    price: number;
    title: string;
    rating: number;
    image : string;

}

const products: Product[] = [

    {
        "id": 1,
        "product_name": "Apple iPhone 13",
        "price": 999.99,
        "title": "The latest iPhone with amazing features.",
        "rating": 4.9,
        "image" : 'http://localhost:3000/pro-4.jpg'
    },
    {
        "id": 2,
        "product_name": "Samsung Galaxy Note 20",
        "price": 899.99,
        "title": "Unleash the power of productivity.",
        "rating": 4.7, 
         "image" : 'http://localhost:3000/product_3.webp'
    },
    {
        "id": 3,
        "product_name": "Google Pixel 6",
        "price": 799.99,
        "title": "Capture perfect shots with AI.",
        "rating": 4.6,
        "image" : 'http://localhost:3000/product_2.webp'
    },
    {
        "id": 4,
        "product_name": "Dell XPS 13",
        "price": 1499.99,
        "title": "Powerful performance in a sleek design.",
        "rating": 4.8,
        "image" : 'http://localhost:3000/pro-3.jpg'
    },
    {
        "id": 5,
        "product_name": "Sony Playstation 5",
        "price": 499.99,
        "title": "Experience next-gen gaming like never before.",
        "rating": 4.9,
        "image" : 'http://localhost:3000/product_4.webp'
    },
    {
        "id": 6,
        "product_name": "Bose QuietComfort 35 II",
        "price": 299.99,
        "title": "Block out the noise and focus on your music.",
        "rating": 4.7,
        "image" : 'http://localhost:3000/product_6.webp'
    },
    {
        "id": 7,
        "product_name": "Fitbit Charge 5",
        "price": 179.99,
        "title": "Stay on top of your fitness game.",
        "rating": 4.5,
        "image" : 'http://localhost:3000/product_8.webp'
    },
    {
        "id": 8,
        "product_name": "Amazon Echo (4th Gen)",
        "price": 99.99,
        "title": "Get hands-free help with Alexa.",
        "rating": 4.6,
        "image" : 'http://localhost:3000/product_7.webp'
    },
    {
        "id": 9,
        "product_name": "GoPro Hero 10 Black",
        "price": 499.99,
        "title": "Capture stunning footage with HyperSmooth 4.0.",
        "rating": 4.8,
        "image" : 'http://localhost:3000/product_10.webp'
    },
    {
        "id": 10,
        "product_name": "Canon EOS R5",
        "price": 3899.99,
        "title": "Revolutionize your photography with 8K video.",
        "rating": 4.9,
        "image" : 'http://localhost:3000/product_9.webp'
    }
]


export default products
