// import { Controller, Get, Param } from '@nestjs/common';

// @Controller('product')
// export class ProductController {
//   constructor() {}

//   @Get()
//   async index(): Promise<boolean> {
//     return true;
//   }
// }
import { Controller, Get, Param } from '@nestjs/common';
// import products, { Product } from 'src/products';
import products, {Product} from '../../products'

@Controller('products')
export class ProductController {
  constructor() {}

  @Get()
  async index(): Promise<Product[]> {
    return products;
  }

  @Get(':id')
 async show(@Param('id') id : string): Promise<Product>{
  return products.find((product) => product.id === parseInt(id));
}
  
}





