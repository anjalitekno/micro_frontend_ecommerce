import React from 'react'
import Footer from './Footer'
import Header from './Header'
import HomeContent from './HomeContent'

const Main = () => {
  return (
    <div>
     Productlist
      <Header/>
      
      <HomeContent/>
      <Footer/>
    </div>
  )
}

export default Main
