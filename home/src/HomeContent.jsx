import React, { useEffect, useState } from "react";

import { getProduct, currency } from "./products";

import { addToCart, useLoggedIn } from 'cart/cart';

import './index.css'

export default function HomeContent() {
    const loggedIn = useLoggedIn();
    const [products, setProducts] = useState([]);

    useEffect(() => {
        getProduct().then(setProducts);
    }, []);


    return (
        <div className="main">

            {products.map((product) =>
            (
                <div key={product.id} className="pro-div" >
                    <p className="pro-text"> {product.id}</p>
                    <p className="pro-text"> {product.product_name}</p>
                    {/* <img src= {product.image}/>  */}
                    <img src={product.image} className="pro-img" />
                    <p className="pro-text"> {product.title}</p>
                    <p className="pro-text"> {product.rating}</p>
                    <p className="pro-text"> {product.price}</p>
                   
                    {loggedIn && (
                        <div className="text-right mt-2">
                            <button
                                className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-bold py-2 px-4 rounded"
                                onClick={() => addToCart(product.id)}
                                id={`addtocart_${product.id}`}
                            >
                                Add to Cart
                            </button>
                        </div>
                    )}
                </div>
            ))}
        </div>
    )
}