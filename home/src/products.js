const API_SERVER = 'http://localhost:3000';

export const getProduct = () => 
fetch(`${API_SERVER}/products`).then((res) => res.json());

export const getProductById = (id) => 
fetch(`${API_SERVER}/products/${id}`).then((res) => res.json());

export const currency = new Intl.NumberFormat("en-Us", { 
    style: 'currency',
    currency: "USD",
})