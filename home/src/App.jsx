import React from "react";
import ReactDOM from "react-dom";
import "remixicon/fonts/remixicon.css"

import "./index.css";
import Main from "./Main";

const App = () => (
  <div className="container">
   <Main/>
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
