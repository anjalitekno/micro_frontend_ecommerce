import React from 'react'
import Footer from 'home/Footer'
import Header from 'home/Header'
import CartContent from './CartContent'

const CartMain = () => {
  return (
    <div>
     Cart
      <Header/>
      <CartContent/>
      <Footer/>
    </div>
  )
}

export default CartMain
