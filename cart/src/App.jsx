import React from "react";
import ReactDOM from "react-dom";
import CartMain from "./CartMain";
import "remixicon/fonts/remixicon.css"

import "./index.css";

const App = () => (
  
  <div className="container">
    <CartMain/>
    
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
