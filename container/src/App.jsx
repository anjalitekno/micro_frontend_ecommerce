import React from "react";
import ReactDOM from "react-dom";
import Main from "home/Main";
import CartMain from "cart/CartMain";
import "./index.css";
import Pdpmaini from "pdp/Pdpmaini";
import "remixicon/fonts/remixicon.css"



const App = () => (
  <>
  <div className="container">
    
    <div className="width">
      <Main />
    </div>
    <div className="width">
      <CartMain />
    </div>


  </div>
  <div className="width">
    <Pdpmaini/>
  </div>
  </>

);
ReactDOM.render(<App />, document.getElementById("app"));
